package msandbox.co.za.patienttrackingsystem.more.api.service;

import com.google.gson.JsonObject;

import msandbox.co.za.patienttrackingsystem.more.api.dao.User;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by takalani on 2016/09/04.
 */
public interface UserService {

    @POST("/users/authenticate")
    Call<JsonObject> authenticate(@Body JsonObject body);
}
