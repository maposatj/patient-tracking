package msandbox.co.za.patienttrackingsystem.more.api.dao;

/**
 * Created by takalani on 2016/09/04.
 */
public class APIError {

    private int statusCode;
    private String error;
    private String message;

    public APIError() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;

    }
}
