package msandbox.co.za.patienttrackingsystem;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.IOException;
import java.lang.annotation.Annotation;

import butterknife.BindView;
import butterknife.ButterKnife;
import msandbox.co.za.patienttrackingsystem.more.api.ErrorUtils;
import msandbox.co.za.patienttrackingsystem.more.api.ServiceGenerator;
import msandbox.co.za.patienttrackingsystem.more.api.dao.APIError;
import msandbox.co.za.patienttrackingsystem.more.api.service.UserService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

//    @BindView(R.id.username)
//    EditText username;
//
//    @BindView(R.id.password)
//    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//
//                final UserService userService = ServiceGenerator.createService(UserService.class);
//
//                JsonObject o = new JsonObject();
//                o.addProperty("username", username.getText().toString());
//                o.addProperty("password", password.getText().toString());
//
//                Call<JsonObject> call = userService.authenticate(o);
//                call.enqueue(new Callback<JsonObject>() {
//                    @Override
//                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//
//                        if (response.isSuccessful()) {
//                            // use response data and do some fancy stuff :)
//                            Toast.makeText(MainActivity.this, "code: " + response.code() + "r: " + String.valueOf(response.body()), Toast.LENGTH_LONG).show();
//                        } else {
//                            // parse the response body …
//                            APIError error = ErrorUtils.parseError(response);
//                            // … and use it to show error information
//
//                            // … or just log the issue like we’re doing :)
//                            Toast.makeText(MainActivity.this, "code: " + response.code() + "r: " + error.getMessage(), Toast.LENGTH_LONG).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<JsonObject> call, Throwable t) {
//                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
